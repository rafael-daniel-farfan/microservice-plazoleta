<br />
<div align="center">
<h3 align="center">PLAZOLETA MICROSERVICE</h3>
  <p align="center">
    In this repository, you will find Plazoleta microservice implementation.
  </p>
</div>

### Built With

* ![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
* ![Spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
* ![Gradle](https://img.shields.io/badge/Gradle-02303A.svg?style=for-the-badge&logo=Gradle&logoColor=white)
* ![MySQL](https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white)


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these steps.

### Prerequisites

* JDK 17 [https://jdk.java.net/java-se-ri/17](https://jdk.java.net/java-se-ri/17)
* Gradle [https://gradle.org/install/](https://gradle.org/install/)
* MySQL [https://dev.mysql.com/downloads/installer/](https://dev.mysql.com/downloads/installer/)

### Recommended Tools
* IntelliJ Community [https://www.jetbrains.com/idea/download/](https://www.jetbrains.com/idea/download/)
* Postman [https://www.postman.com/downloads/](https://www.postman.com/downloads/)

### Installation

1. Clone the repository
2. Change directory
   ```sh
   cd microservice-plazoleta
   ```
3. Create a new database in MySQL called restaurant
4. Update the database connection settings
   ```yml
   # src/main/resources/application-dev.yml
   spring:
      datasource:
          url: jdbc:mysql://localhost/restaurant
          username: root
          password: <your-password>
   ```


<!-- USAGE -->

## Usage Postman to adm Restaurants and dishes

1. Open Postman
2. import DISH_REST_CONTROLLER.postman_collection.json and RESTAURANT_CONTROLLERS.postman_collection.json.
3. Go to RESTAURANT CONTROLLERS collection and make a request in http://localhost:8099/restaurant
3. Go to DISH REST CONTROLLER collection and make request in http://localhost:8099/dish to save a dish (POST) 
4. and update a dish in http://localhost:8099/dish/{id} 



<!-- ROADMAP -->
## Tests 

- Right-click the test folder and choose Run tests with coverage
