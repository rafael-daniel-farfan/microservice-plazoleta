package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.CategoryEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.DishEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.DishNotFoundExc;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.ICategoryEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IDishEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IDishRepository;
import com.pragma.powerup.usermicroservice.domain.model.Dish;
import com.pragma.powerup.usermicroservice.domain.spi.IDishPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;


import java.util.*;

import static com.pragma.powerup.usermicroservice.configuration.Constants.MAX_PAGE_SIZE;


@RequiredArgsConstructor
public class DishMySqlAdapter implements IDishPersistencePort {

    private final IDishRepository iDishRepository;

    private final IDishEntityMapper iDishEntityMapper;

    private final ICategoryEntityMapper iCategoryEntityMapper;


    @Override
    public void saveDish(Dish dish) {

        iDishRepository.save(iDishEntityMapper.toEntity(dish));
    }

    @Override
    public void updateDish(Dish dish, Long id) {
        Optional<DishEntity> optional = Optional.of(iDishRepository.findById(id).orElseThrow(null));

        if (!optional.isPresent()) {
            throw new DishNotFoundExc();
        }
        DishEntity dishEntity = optional.get();
        dishEntity.setPrice(dish.getPrice());
        dishEntity.setDescription(dish.getDescription());

        iDishRepository.save(dishEntity);

    }
    @Override
    public void enableDish(Long id) {
        Optional<DishEntity> optional = iDishRepository.findById(id);
        DishEntity dishEntity = optional.get();

        if (!optional.isPresent()) {

            throw new DishNotFoundExc();
        }
        if (dishEntity.isActive()) {
            dishEntity.setActive(false);
            iDishRepository.save(dishEntity);
        } else {
            dishEntity.setActive(true);
            iDishRepository.save(dishEntity);
        }
    }

    @Override
    public List<Dish> findByRestaurantId(Long restaurantId, int page) {
        Pageable pagination = PageRequest.of(page, MAX_PAGE_SIZE);
        List<DishEntity> dishEntities = iDishRepository.findByRestaurantId(restaurantId, pagination).getContent();

        Map<CategoryEntity, List<DishEntity>> dishByCategory = new HashMap<>();
        for (DishEntity dishEntity : dishEntities) {
            CategoryEntity category = dishEntity.getCategory();
            List<DishEntity> platosCategoria = dishByCategory.getOrDefault(category, new ArrayList<>());
            platosCategoria.add(dishEntity);
            dishByCategory.put(category, platosCategoria);
        }
        List<Dish> dishList = new ArrayList<>();
        for (Map.Entry<CategoryEntity, List<DishEntity>> entry : dishByCategory.entrySet()) {
            CategoryEntity categoria = entry.getKey();
            List<DishEntity> platosCategoria = entry.getValue();

            List<Dish> listaPlatosCategoria = iDishEntityMapper.toDishList(platosCategoria);

            for (Dish plato : listaPlatosCategoria) {
                plato.setCategory(iCategoryEntityMapper.toCategory(categoria));
            }

            dishList.addAll(listaPlatosCategoria);
        }

        return dishList;


    }
}


