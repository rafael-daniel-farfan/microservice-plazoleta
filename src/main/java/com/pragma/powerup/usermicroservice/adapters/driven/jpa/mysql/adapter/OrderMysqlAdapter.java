package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.DishEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.OrderEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.TicketEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.OrderActiveException;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IDishEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IOrderEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IOrderRepository;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.ITicketRepository;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.utils.StateOrder;
import com.pragma.powerup.usermicroservice.adapters.driving.http.clients.IPlazoletaClienteRest;

import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IOrderResponseMapper;

import com.pragma.powerup.usermicroservice.domain.model.Dish;
import com.pragma.powerup.usermicroservice.domain.model.Order;
import com.pragma.powerup.usermicroservice.domain.spi.IOrderPersistencePort;

import com.pragma.powerup.usermicroservice.domain.utils.State;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static com.pragma.powerup.usermicroservice.configuration.Constants.MAX_PAGE_SIZE;

@RequiredArgsConstructor
public class OrderMysqlAdapter implements IOrderPersistencePort {
    private final IOrderRepository orderRepository;
    private final IOrderEntityMapper orderEntityMapper;

    private final IOrderResponseMapper orderResponseMapper;

    private final IDishEntityMapper dishEntityMapper;

    private final ITicketRepository ticketRepository;

    @Autowired
    private IPlazoletaClienteRest client;

    @Override
    public void createOrder(Order order) {

        List<OrderEntity> ordersByUser = orderRepository.findAllByIdUser(order.getIdUser());
        boolean hasPendingState = false;
        for (OrderEntity orderEntity : ordersByUser) {
            StateOrder state = orderEntity.getState();
            if (state == StateOrder.PENDING || state == StateOrder.PREPARING || state == StateOrder.READY_FOR_DELIVERY) {
                hasPendingState = true;
                throw new OrderActiveException();
            }
        }

        if (!hasPendingState) {

            List<Dish> dishes = order.getDishes();
            OrderEntity orderSave = orderEntityMapper.toEntity(order);
            orderRepository.save(orderSave);

            for (Dish dish : dishes) {
                OrderEntity orderTicket = new OrderEntity();
                orderTicket.setId(orderSave.getId());

                DishEntity dishSave = dishEntityMapper.toEntity(dish);

                TicketEntity ticket = new TicketEntity();
                ticket.setOrderEntity(orderTicket);
                ticket.setDishEntity(dishSave);

                ticketRepository.save(ticket);
            }
        }

    }

    @Override
    public List<Order> findByRestaurantIdAndState(Long restaurantId, StateOrder state, int page) {
        Pageable pagination = PageRequest.of(page, MAX_PAGE_SIZE);
        List<OrderEntity> orders = orderRepository.findByRestaurantIdAndState(restaurantId, state, pagination).getContent();
        return orderEntityMapper.toOrderList(orders);
    }
}
