package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestaurantEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IRestaurantEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IRestaurantRepository;
import com.pragma.powerup.usermicroservice.domain.model.Restaurant;
import com.pragma.powerup.usermicroservice.domain.spi.IRestaurantPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

import static com.pragma.powerup.usermicroservice.configuration.Constants.MAX_PAGE_SIZE;


@RequiredArgsConstructor
public class RestaurantMySqlAdapter implements IRestaurantPersistencePort {

    private final IRestaurantRepository restaurantRepository;
    private final IRestaurantEntityMapper restaurantEntityMapper;


    @Override
    public void saveRestaurant(Restaurant restaurant) {

        restaurantRepository.save(restaurantEntityMapper.toEntity(restaurant));

    }
    @Override
    public List<Restaurant> findAll(int page) {
        Pageable pagination = PageRequest.of(page, MAX_PAGE_SIZE, Sort.by("name").ascending());
        List<RestaurantEntity> restaurantEntityList = restaurantRepository.findAll(pagination).getContent();
        return restaurantEntityMapper.toRestaurantList(restaurantEntityList);
    }
}
