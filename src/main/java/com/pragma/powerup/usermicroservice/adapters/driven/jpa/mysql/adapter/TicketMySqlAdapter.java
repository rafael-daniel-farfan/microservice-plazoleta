package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.ITicketEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.ITicketRepository;
import com.pragma.powerup.usermicroservice.domain.model.Ticket;
import com.pragma.powerup.usermicroservice.domain.spi.ITicketPersistencePort;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class TicketMySqlAdapter  implements ITicketPersistencePort {

    private final ITicketRepository ticketRepository;
    private final ITicketEntityMapper iTicketEntityMapper;


    @Override
    public void createTicket(Ticket ticket) {

        ticketRepository.save(iTicketEntityMapper.toEntity(ticket));

    }
}
