package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "dishes")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DishEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(updatable = false)
    private String nameDish;
    private Long price;
    private String Description;
    @Column(updatable = false)
    private String urlImage;
    @ManyToOne
    @JoinColumn(name = "restaurant_id", updatable = false)
    private RestaurantEntity restaurant;
    @ManyToOne
    @JoinColumn(name = "category_id", updatable = false)
    private CategoryEntity category;
    private boolean active;
    @OneToMany(mappedBy = "dishEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TicketEntity> orders;

}
