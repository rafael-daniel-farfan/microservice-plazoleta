package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.utils.StateOrder;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "orders")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long idUser;
    private LocalDate orderDate;
    @Enumerated(EnumType.STRING)
    private StateOrder state;
    @ManyToOne
    @JoinColumn(name = "restaurant_id", updatable = false)
    private RestaurantEntity restaurant;
    private Long idChef;
    @OneToMany(mappedBy = "orderEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TicketEntity> dishes;
    private Long quantity;




}
