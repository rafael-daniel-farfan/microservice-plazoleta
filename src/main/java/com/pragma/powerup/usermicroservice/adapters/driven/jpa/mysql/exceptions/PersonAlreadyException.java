package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions;

public class PersonAlreadyException extends RuntimeException {
    public PersonAlreadyException() {
        super();
    }
}
