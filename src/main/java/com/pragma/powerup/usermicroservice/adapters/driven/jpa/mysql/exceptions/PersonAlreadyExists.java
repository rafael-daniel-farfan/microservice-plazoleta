package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions;

public class PersonAlreadyExists extends RuntimeException {
    public PersonAlreadyExists() {
        super();
    }
}
