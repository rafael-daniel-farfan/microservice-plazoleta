package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions;

public class RoleNotAllowed extends RuntimeException {
    public RoleNotAllowed() {
        super();
    }
}
