package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions;

public class RoleNotAllowedException extends RuntimeException {
    public RoleNotAllowedException() {
        super();
    }
}
