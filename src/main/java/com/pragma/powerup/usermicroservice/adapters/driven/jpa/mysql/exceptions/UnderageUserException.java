package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions;

public class UnderageUserException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private static final String DESCRIPTION = "User cannot be underage";
    public UnderageUserException() {
        super(DESCRIPTION );
    }
}
