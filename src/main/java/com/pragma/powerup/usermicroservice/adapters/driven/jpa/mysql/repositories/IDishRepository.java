package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.DishEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IDishRepository extends JpaRepository<DishEntity, Long> {

    Optional<DishEntity> findByNameDish(String nameDish);
    boolean existsByNameDish(String nameDish);
//    Page<DishEntity> findAll(Pageable pageable);
    Page<DishEntity> findByRestaurantId(Long restaurantId, Pageable pageable);


}
