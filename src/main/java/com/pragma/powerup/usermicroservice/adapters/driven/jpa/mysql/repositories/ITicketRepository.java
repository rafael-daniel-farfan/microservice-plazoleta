package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.TicketEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITicketRepository extends JpaRepository <TicketEntity, Long> {


}
