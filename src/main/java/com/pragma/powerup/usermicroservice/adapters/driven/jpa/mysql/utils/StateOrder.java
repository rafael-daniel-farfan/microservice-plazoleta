package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.utils;

public enum StateOrder {

    PENDING, PREPARING, READY_FOR_DELIVERY, COMPLETED


}
