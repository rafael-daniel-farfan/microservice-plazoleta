package com.pragma.powerup.usermicroservice.adapters.driving.http.clients;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.PersonResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.configuration.security.feignconfig.FeignClientConfig;

import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.*;



@FeignClient(name ="microservice-user",  url="http://localhost:8090/user", configuration = FeignClientConfig.class)
public interface IPlazoletaClienteRest {

    @GetMapping("/{id}")
    public UserResponseDto findById(@PathVariable long id);
    @GetMapping("/personByUser/{id}")
    public PersonResponseDto findPersonByIdUser(@PathVariable long id);

    @PutMapping
    public void updateUser (@RequestBody UserRequestDto userRequestDto);






}