package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DishRequestDto {

    private Long id;
    private String nameDish;
    private Long price;
    private String Description;
    private String urlImage;
    @NotNull(message = "You must associate a restaurant")
    private RestaurantRequestDto restaurant;
    private CategoryRequestDto category;
}
