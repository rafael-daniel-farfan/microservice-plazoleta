package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;


import com.pragma.powerup.usermicroservice.adapters.driving.http.utils.StateHandler;

import com.pragma.powerup.usermicroservice.domain.model.Dish;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Getter @Setter
public class OrderRequestDto {


    private Long id;
    private Long idUser;
    private LocalDate orderDate;
    private StateHandler state;
    private RestaurantRequestDto restaurant;
    private Long idChef;
    private List<Dish> dishes;
}
