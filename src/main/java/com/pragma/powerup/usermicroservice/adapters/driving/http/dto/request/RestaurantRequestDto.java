package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RestaurantRequestDto {

    private Long id;
    private String name;
    private Long nit;
    private String address;
    @Size(min = 7, max = 13)
    @Pattern(regexp = "^\\+?[0-9]+$", message = "Número incorrecto. El campo 'telefono' debe ser numérico y puede contener el prefijo +57")
    private String phone;
    private String urlLogo;
    private Long idOwner;
}
