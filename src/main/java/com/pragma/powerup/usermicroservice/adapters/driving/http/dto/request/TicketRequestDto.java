package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class TicketRequestDto {


    private Long id;
    private OrderRequestDto orderEntity;
    private DishRequestDto dishEntity;
}
