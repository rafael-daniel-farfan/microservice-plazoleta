package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.OrderResponseDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class UserRequestDto {
    private Long id;
    private Long idPerson;
    private Long idRole;
    private List<OrderResponseDto> orders;
}
