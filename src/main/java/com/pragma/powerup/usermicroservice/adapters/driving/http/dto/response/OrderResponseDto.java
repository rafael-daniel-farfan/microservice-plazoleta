package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response;

import com.pragma.powerup.usermicroservice.adapters.driving.http.utils.StateHandler;
import com.pragma.powerup.usermicroservice.domain.model.Dish;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Getter
public class OrderResponseDto {

    private Long id;
    private Long idUser;
    private LocalDate orderDate;
    private StateHandler state;
    private RestaurantResponseDto restaurant;
    private Long idChef;
    private List<Dish> dishes;
}
