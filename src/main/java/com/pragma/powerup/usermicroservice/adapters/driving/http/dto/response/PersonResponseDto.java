package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@AllArgsConstructor
@Getter
public class PersonResponseDto {
    private Long id;
    private String name;
    private String surname;
    private String mail;
    private String phone;
    private LocalDate birthDate;
    private String address;
    private String idDniType;
    private Long dniNumber;
    private String idPersonType;

}
