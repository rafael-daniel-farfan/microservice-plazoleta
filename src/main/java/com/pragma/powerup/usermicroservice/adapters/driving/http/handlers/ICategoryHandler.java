package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.CategoryRequestDto;

public interface ICategoryHandler {

    void saveCategory(CategoryRequestDto categoryRequestDto);
}
