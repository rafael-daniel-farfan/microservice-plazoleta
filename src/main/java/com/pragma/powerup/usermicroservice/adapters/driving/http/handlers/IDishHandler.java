package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.DishRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.DishUpdateRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.DishResponseDto;

import java.util.List;


public interface IDishHandler {

    void saveDish(DishRequestDto dishRequestDto);

    void updateDish(DishUpdateRequestDto dishUpdateRequestDto, Long id);

    void enableDish(Long id);

    List<DishResponseDto> findByRestaurantId (Long restaurantId, Integer page);
}
