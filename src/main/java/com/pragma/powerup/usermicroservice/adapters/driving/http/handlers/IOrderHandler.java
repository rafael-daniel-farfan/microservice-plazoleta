package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.utils.StateOrder;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.OrderRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.OrderResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.utils.StateHandler;

import java.util.List;

public interface IOrderHandler {

    void createOrder(OrderRequestDto orderRequestDto);

    List<OrderResponseDto> findByRestaurantIdAndState(Long restaurantId, StateOrder state, Integer page );
}
