package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.TicketRequestDto;

public interface ITicketHandler {

    void createTicket(TicketRequestDto ticketRequestDto);
}
