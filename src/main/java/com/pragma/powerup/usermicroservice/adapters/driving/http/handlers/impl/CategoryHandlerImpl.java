package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.CategoryRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.ICategoryHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.ICategoryRequestMapper;
import com.pragma.powerup.usermicroservice.domain.api.ICategoryServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CategoryHandlerImpl implements ICategoryHandler {

    private final ICategoryServicePort iCategoryServicePort;

    private final ICategoryRequestMapper iCategoryRequestMapper;

    @Override
    public void saveCategory(CategoryRequestDto categoryRequestDto) {
        iCategoryServicePort.saveCategory(iCategoryRequestMapper.toCategory(categoryRequestDto));
    }


}
