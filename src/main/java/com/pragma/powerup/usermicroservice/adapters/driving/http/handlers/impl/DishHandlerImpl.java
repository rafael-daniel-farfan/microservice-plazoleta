package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.DishRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.DishUpdateRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.DishResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IDishHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IDishRequestMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IDishResponseMapper;
import com.pragma.powerup.usermicroservice.domain.api.IDishServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DishHandlerImpl implements IDishHandler {

    private final IDishServicePort iDishServicePort;
    private final IDishRequestMapper iDishRequestMapper;
    private final IDishResponseMapper iDishResponseMapper;

    @Override
    public void saveDish(DishRequestDto dishRequestDto) {
        iDishServicePort.saveDish(iDishRequestMapper.toDish(dishRequestDto));

    }
    @Override
    public void updateDish(DishUpdateRequestDto dishUpdateRequestDto, Long id) {

        iDishServicePort.updateDish(iDishRequestMapper.toDishUp(dishUpdateRequestDto), id);

    }

    @Override
    public void enableDish( Long id) {
        iDishServicePort.enableDish( id);
    }

    @Override
    public List<DishResponseDto> findByRestaurantId(Long restaurantId, Integer page) {
        return iDishResponseMapper.dishListToDishResponseList(iDishServicePort.findByRestaurantId(restaurantId, page));
    }
}
