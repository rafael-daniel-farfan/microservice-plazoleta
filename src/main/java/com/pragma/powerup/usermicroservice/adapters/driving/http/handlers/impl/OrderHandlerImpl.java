package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.utils.StateOrder;
import com.pragma.powerup.usermicroservice.adapters.driving.http.clients.IPlazoletaClienteRest;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.OrderRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.OrderResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IOrderHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IOrderRequestMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IOrderResponseMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.utils.StateHandler;
import com.pragma.powerup.usermicroservice.domain.api.IOrderServicePort;
import com.pragma.powerup.usermicroservice.domain.exceptions.UserNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderHandlerImpl implements IOrderHandler {
    private final IOrderServicePort orderServicePort;
    private final IOrderRequestMapper orderRequestMapper;
    private final IOrderResponseMapper orderResponseMapper;
    @Autowired
    private IPlazoletaClienteRest client;
    @Override
    public void createOrder(OrderRequestDto orderRequestDto) {

        UserResponseDto user = client.findById(orderRequestDto.getIdUser());

        if (user == null) {
            throw new UserNotFoundException();
        }

        orderServicePort.createOrder(orderRequestMapper.toOrder(orderRequestDto));
    }
    @Override
    public List<OrderResponseDto> findByRestaurantIdAndState(Long restaurantId, StateOrder state, Integer page) {
        return orderResponseMapper.orderListToOrderResponseDto(orderServicePort.findByRestaurantIdAndState(restaurantId, state, page));
    }

}

