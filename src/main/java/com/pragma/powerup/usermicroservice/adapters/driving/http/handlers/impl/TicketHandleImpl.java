package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.TicketRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.ITicketHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.ITicketRequestMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.ITicketResponseMapper;
import com.pragma.powerup.usermicroservice.domain.api.ITicketServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TicketHandleImpl implements ITicketHandler {

    private final ITicketServicePort iTicketServicePort;
    private final ITicketRequestMapper iTicketRequestMapper;
    private final ITicketResponseMapper iTicketResponseMapper;
    @Override
    public void createTicket(TicketRequestDto ticketRequestDto) {
        iTicketServicePort.createTicket(iTicketRequestMapper.toTicket(ticketRequestDto));
    }
}
