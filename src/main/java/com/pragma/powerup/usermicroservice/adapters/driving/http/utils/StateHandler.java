package com.pragma.powerup.usermicroservice.adapters.driving.http.utils;

public enum StateHandler {

    PENDING, PREPARING, READY_FOR_DELIVERY, COMPLETED
}
