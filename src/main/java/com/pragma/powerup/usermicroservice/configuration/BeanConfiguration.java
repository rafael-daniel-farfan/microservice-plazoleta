package com.pragma.powerup.usermicroservice.configuration;


import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter.*;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.*;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.*;

import com.pragma.powerup.usermicroservice.adapters.driving.http.clients.IPlazoletaClienteRest;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IOrderResponseMapper;
import com.pragma.powerup.usermicroservice.domain.api.*;

import com.pragma.powerup.usermicroservice.domain.spi.*;

import com.pragma.powerup.usermicroservice.domain.usecase.*;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    private final IRestaurantRepository restaurantRepository;
    private final IRestaurantEntityMapper restaurantEntityMapper;

    private IPlazoletaClienteRest plazoletaClienteRest;

    private final IDishRepository iDishRepository;

    private final IDishEntityMapper iDishEntityMapper;

    private final ICategoryRepository iCategoryRepository;

    private final ICategoryEntityMapper iCategoryEntityMapper;

    private final IOrderRepository orderRepository;

    private final IOrderEntityMapper orderEntityMapper;

    private final ITicketRepository ticketRepository;

    private final ITicketEntityMapper iTicketEntityMapper;

    private final IDishEntityMapper dishEntityMapper;

    private final IOrderResponseMapper orderResponseMapper;




    @Bean
    public IRestaurantServicePort restaurantServicePort() {

        return new RestaurantUseCase(restaurantPersistencePort(), plazoletaClienteRest);
    }
    @Bean
    public IRestaurantPersistencePort restaurantPersistencePort() {

        return new RestaurantMySqlAdapter(restaurantRepository, restaurantEntityMapper);
    }

    @Bean
    public IDishServicePort iDishServicePort() {

        return new DishUseCase(dishPersistencePort(), plazoletaClienteRest);
    }

    @Bean
    public IDishPersistencePort dishPersistencePort() {
        return new DishMySqlAdapter(iDishRepository,iDishEntityMapper, iCategoryEntityMapper);
    }

    @Bean
    public ICategoryServicePort iCategoryServicePort(){

        return new CategoryUseCase(categoryPersistencePort());
    }

    @Bean
    public ICategoryPersistencePort categoryPersistencePort() {
        return new CategoryMySqlAdapter(iCategoryRepository,iCategoryEntityMapper);
    }

    @Bean
    public IOrderServicePort orderServicePort() {
        return new OrderUseCase(orderPersistencePort());
    }

    @Bean
    public IOrderPersistencePort orderPersistencePort() {
        return new OrderMysqlAdapter(orderRepository, orderEntityMapper, orderResponseMapper, dishEntityMapper, ticketRepository);
    }
    @Bean
    public ITicketServicePort ticketServicePort() {
        return new TicketUseCase(ticketPersistencePort());
    }

    @Bean
    public ITicketPersistencePort ticketPersistencePort(){
        return new TicketMySqlAdapter(ticketRepository, iTicketEntityMapper );
    }

}
