package com.pragma.powerup.usermicroservice.configuration.seguridad.jwt;



import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Component
public class JwtTokenFilter extends OncePerRequestFilter {
    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private int expiration;

    private List<String> excludedPrefixes = Arrays.asList("/auth/**",  "/actuator/**", "/person/");

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String token = getToken(request);
            if (token != null && validateToken(token)) {
                if (getRoles(token).contains("ROLE_OWNER") && isAllowedByOwner(request)) {
                    filterChain.doFilter(request, response);
                } else if (request.getRequestURI().contains("/swagger-ui")){
                    filterChain.doFilter(request, response);
                } else if (getRoles(token).contains("ROLE_OWNER") && request.getRequestURI().contains("/id/{id}")) {
                    filterChain.doFilter(request, response);
                } else if (getRoles(token).contains("ROLE_ADMIN")) {
                    filterChain.doFilter(request, response);
                }  else if (getRoles(token).contains("ROLE_CLIENT") && request.getRequestURI().contains("/restaurant")) {
                    filterChain.doFilter(request, response);
                } else if (getRoles(token).contains("ROLE_CLIENT") && request.getRequestURI().contains("/dish")) {
                    filterChain.doFilter(request, response);
                }else if (getRoles(token).contains("ROLE_CLIENT") && request.getRequestURI().equals("/order")) {
                    filterChain.doFilter(request, response);
                }else if (getRoles(token).contains("ROLE_EMPLOYEE") && request.getRequestURI().contains("/order")) {
                    filterChain.doFilter(request, response);
                }else {
                    sendUnauthorizedError(response);
                }
            } else {
                sendUnauthorizedError(response);
            }
        } catch (Exception e) {
            sendUnauthorizedError(response);
        }
    }

    private void sendUnauthorizedError(HttpServletResponse response) throws IOException {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
    }

    public String getNombreUsuarioFromToken(String token) {
        return Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token).getBody().getSubject();
    }

    private boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token);
            return true;
        } catch (MalformedJwtException e) {
            logger.error("token mal formado");
        } catch (UnsupportedJwtException e) {
            logger.error("token no soportado");
        } catch (ExpiredJwtException e) {
            logger.error("token expirado");
        } catch (IllegalArgumentException e) {
            logger.error("token vacío");
        } catch (SignatureException e) {
            logger.error("fail en la firma");
        }
        return false;
    }

    private List<String> getRoles(String token){
            Claims claims = Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token).getBody();
            return claims.get("roles", List.class);
    }

    public String getToken(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        if (header != null && header.startsWith("Bearer ")) {
            return header.substring(7); // return everything after "Bearer "
        }
        return null;
    }

    private boolean isAllowedByOwner(HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        List<String> allowedPaths = Arrays.asList("/dish", "/dish/{id}", "/dish/enable/{id}");

        AntPathMatcher pathMatcher = new AntPathMatcher();
        return allowedPaths.stream()
                .anyMatch(pattern -> pathMatcher.match(pattern, requestURI));
    }


}
