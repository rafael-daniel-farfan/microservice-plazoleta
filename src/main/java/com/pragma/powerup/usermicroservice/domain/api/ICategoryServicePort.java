package com.pragma.powerup.usermicroservice.domain.api;

import com.pragma.powerup.usermicroservice.domain.model.Category;

public interface ICategoryServicePort {

    void saveCategory(Category category);
}
