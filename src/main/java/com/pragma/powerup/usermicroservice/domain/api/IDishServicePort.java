package com.pragma.powerup.usermicroservice.domain.api;


import com.pragma.powerup.usermicroservice.domain.model.Dish;

import java.util.List;

public interface IDishServicePort {

    void saveDish(Dish dish);

    void updateDish(Dish dish, Long id);

    void enableDish( Long id);

    List<Dish> findByRestaurantId(Long restaurantId, int page);


}
