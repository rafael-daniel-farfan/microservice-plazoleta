package com.pragma.powerup.usermicroservice.domain.api;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.utils.StateOrder;
import com.pragma.powerup.usermicroservice.domain.model.Order;
import com.pragma.powerup.usermicroservice.domain.utils.State;

import java.util.List;

public interface IOrderServicePort {

    void createOrder(Order order);

    List<Order> findByRestaurantIdAndState(Long restaurantId, StateOrder state, int page);
}
