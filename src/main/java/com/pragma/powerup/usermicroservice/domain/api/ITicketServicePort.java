package com.pragma.powerup.usermicroservice.domain.api;

import com.pragma.powerup.usermicroservice.domain.model.Ticket;

public interface ITicketServicePort {

    void createTicket(Ticket ticket);
}
