package com.pragma.powerup.usermicroservice.domain.exceptions;

public class RestaurantEmptyException extends RuntimeException {

    public RestaurantEmptyException() {
        super();
    }
}
