package com.pragma.powerup.usermicroservice.domain.model;

public class Category {

    private Long id;
    private String nameCategory;
    private String descriptionCategory;

    public Category(Long id, String nameCategory, String descriptionCategory) {
        this.id = id;
        this.nameCategory = nameCategory;
        this.descriptionCategory = descriptionCategory;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public String getDescriptionCategory() {
        return descriptionCategory;
    }

    public void setDescriptionCategory(String descriptionCategory) {
        this.descriptionCategory = descriptionCategory;
    }
}
