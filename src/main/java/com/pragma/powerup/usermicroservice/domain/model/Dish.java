package com.pragma.powerup.usermicroservice.domain.model;

public class Dish {


    private Long id;
    private String nameDish;
    private Long price;
    private String Description;
    private String urlImage;
    private Restaurant restaurant;
    private Category category;

    private boolean active;

    public Dish(Long id, String nameDish, Long price, String description, String urlImage, Restaurant restaurant, Category category, boolean active) {
        this.id = id;
        this.nameDish = nameDish;
        this.price = price;
        this.Description = description;
        this.urlImage = urlImage;
        this.restaurant = restaurant;
        this.category = category;
        this.active = active;
    }


    public Dish() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameDish() {
        return nameDish;
    }

    public void setNameDish(String nameDish) {
        this.nameDish = nameDish;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
