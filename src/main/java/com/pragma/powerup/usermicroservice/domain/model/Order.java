package com.pragma.powerup.usermicroservice.domain.model;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestaurantEntity;
import com.pragma.powerup.usermicroservice.domain.utils.State;


import java.time.LocalDate;
import java.util.List;

public class Order {


    private Long id;
    private Long idUser;
    private LocalDate orderDate;
    private State state;
    private RestaurantEntity restaurant;
    private Long idChef;
    private List<Dish> dishes;
    private Long quantity;



    public Order(Long id, Long idUser, LocalDate orderDate, State state, RestaurantEntity restaurant, Long idChef, List<Dish> dishes, Long quantity) {
        this.id = id;
        this.idUser = idUser;
        this.orderDate = orderDate;
        this.state = state;
        this.restaurant = restaurant;
        this.idChef = idChef;
        this.dishes = dishes;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public RestaurantEntity getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(RestaurantEntity restaurant) {
        this.restaurant = restaurant;
    }

    public Long getIdChef() {
        return idChef;
    }

    public void setIdChef(Long idChef) {
        this.idChef = idChef;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }


}
