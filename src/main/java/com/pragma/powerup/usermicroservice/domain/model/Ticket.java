package com.pragma.powerup.usermicroservice.domain.model;


import lombok.Builder;

public class Ticket {
    private Long id;
    private Order order;
    private Dish dish;

    public Ticket(Order order, Dish dish) {
        this.order = order;
        this.dish = dish;
    }
    @Builder
    public Ticket(Long id, Order order, Dish dish) {
        this.id = id;
        this.order = order;
        this.dish = dish;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }
}
