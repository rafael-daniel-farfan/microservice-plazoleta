package com.pragma.powerup.usermicroservice.domain.spi;

import com.pragma.powerup.usermicroservice.domain.model.Category;

public interface ICategoryPersistencePort {

    void saveCategory(Category category);
}
