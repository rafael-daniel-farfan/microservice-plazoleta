package com.pragma.powerup.usermicroservice.domain.spi;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.utils.StateOrder;
import com.pragma.powerup.usermicroservice.domain.model.Order;
import com.pragma.powerup.usermicroservice.domain.utils.State;

import java.util.List;

public interface IOrderPersistencePort {

    void createOrder(Order order);

    List<Order> findByRestaurantIdAndState(Long restaurantId, StateOrder state, int page);
}
