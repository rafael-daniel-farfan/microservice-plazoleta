package com.pragma.powerup.usermicroservice.domain.spi;

import com.pragma.powerup.usermicroservice.domain.model.Ticket;

public interface ITicketPersistencePort {

    void createTicket(Ticket ticket);
}
