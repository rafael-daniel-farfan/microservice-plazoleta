package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.adapters.driving.http.clients.IPlazoletaClienteRest;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.domain.api.IDishServicePort;
import com.pragma.powerup.usermicroservice.domain.exceptions.RestaurantEmptyException;
import com.pragma.powerup.usermicroservice.domain.exceptions.RoleNotAllowedForCreationException;
import com.pragma.powerup.usermicroservice.domain.model.Dish;
import com.pragma.powerup.usermicroservice.domain.spi.IDishPersistencePort;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class DishUseCase implements IDishServicePort {

    private final IDishPersistencePort dishPersistencePort;
    @Autowired
    private IPlazoletaClienteRest plazoletaClienteRest;


    public DishUseCase(IDishPersistencePort dishPersistencePort, IPlazoletaClienteRest plazoletaClienteRest) {
        this.dishPersistencePort = dishPersistencePort;
        this.plazoletaClienteRest=plazoletaClienteRest;
    }
    @Override
    public void saveDish(Dish dish) {
        if(dish.getRestaurant().getId() == null){
            throw new RestaurantEmptyException();
        }

        Long idOwner = dish.getRestaurant().getIdOwner();
        UserResponseDto user= plazoletaClienteRest.findById(idOwner);

        if(user.getIdRole() !=3){

            throw new RoleNotAllowedForCreationException();
        }

        dish.setActive(true);
        dishPersistencePort.saveDish(dish);

    }

    @Override
    public void updateDish(Dish dish, Long id) {

        dishPersistencePort.updateDish(dish, id);

    }
    @Override
    public void enableDish(Long id) {

        dishPersistencePort.enableDish(id);

    }

    @Override
    public List<Dish> findByRestaurantId(Long restaurantId, int page) {
        return dishPersistencePort.findByRestaurantId(restaurantId, page);
    }

}
