package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.utils.StateOrder;
import com.pragma.powerup.usermicroservice.domain.api.IOrderServicePort;
import com.pragma.powerup.usermicroservice.domain.model.Dish;
import com.pragma.powerup.usermicroservice.domain.model.Order;
import com.pragma.powerup.usermicroservice.domain.spi.IOrderPersistencePort;
import com.pragma.powerup.usermicroservice.domain.utils.State;


import java.time.LocalDate;

import java.util.List;

public class OrderUseCase implements IOrderServicePort {
    private final IOrderPersistencePort orderPersistencePort;

    public OrderUseCase(IOrderPersistencePort orderPersistencePort) {
        this.orderPersistencePort = orderPersistencePort;

    }

    @Override
    public void createOrder(Order order) {

        List<Dish> dishes = order.getDishes();
        order.setOrderDate(LocalDate.now());
        order.setState(State.PENDING);
        order.setQuantity((long) dishes.size());
        orderPersistencePort.createOrder(order);


    }

    @Override
    public List<Order> findByRestaurantIdAndState(Long restaurantId, StateOrder state, int page) {
        return orderPersistencePort.findByRestaurantIdAndState(restaurantId, state, page);
    }

}
