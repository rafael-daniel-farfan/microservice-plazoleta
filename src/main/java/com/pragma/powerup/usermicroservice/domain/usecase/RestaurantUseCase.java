package com.pragma.powerup.usermicroservice.domain.usecase;


import com.pragma.powerup.usermicroservice.adapters.driving.http.clients.IPlazoletaClienteRest;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.domain.api.IRestaurantServicePort;
import com.pragma.powerup.usermicroservice.domain.exceptions.RoleNotAllowedForCreationException;
import com.pragma.powerup.usermicroservice.domain.exceptions.UserNotFoundException;
import com.pragma.powerup.usermicroservice.domain.model.Restaurant;
import com.pragma.powerup.usermicroservice.domain.spi.IRestaurantPersistencePort;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;



public class RestaurantUseCase implements IRestaurantServicePort {

    private final IRestaurantPersistencePort restaurantPersistencePort;

    @Autowired
    private IPlazoletaClienteRest client;


    public RestaurantUseCase(IRestaurantPersistencePort restaurantPersistencePort, IPlazoletaClienteRest client) {
        this.restaurantPersistencePort = restaurantPersistencePort;
        this.client = client;
           }

    @Override
    public void saveRestaurant(Restaurant restaurant) {

        UserResponseDto user= client.findById(restaurant.getIdOwner());

        if(user == null){
            throw new UserNotFoundException();
        }
        if(user.getIdRole() != 3){

            throw new RoleNotAllowedForCreationException();
        }
        restaurantPersistencePort.saveRestaurant(restaurant);

    }

    @Override
    public List<Restaurant> findAll(int page) {

        return restaurantPersistencePort.findAll(page);
    }


}
