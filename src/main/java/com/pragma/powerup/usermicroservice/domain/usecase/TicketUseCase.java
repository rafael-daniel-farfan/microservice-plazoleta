package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.domain.api.ITicketServicePort;
import com.pragma.powerup.usermicroservice.domain.model.Ticket;
import com.pragma.powerup.usermicroservice.domain.spi.ITicketPersistencePort;

public class TicketUseCase implements ITicketServicePort {

    private final ITicketPersistencePort ticketPersistencePort;

    public TicketUseCase(ITicketPersistencePort ticketPersistencePort) {
        this.ticketPersistencePort = ticketPersistencePort;
    }

    @Override
    public void createTicket(Ticket ticket) {

        ticketPersistencePort.createTicket(ticket);

    }
}
