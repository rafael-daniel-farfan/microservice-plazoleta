package com.pragma.powerup.usermicroservice.domain.utils;

public enum State {
    PENDING, PREPARING, READY_FOR_DELIVERY, COMPLETED
}
