package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.adapters.driving.http.clients.IPlazoletaClienteRest;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.domain.exceptions.RestaurantEmptyException;
import com.pragma.powerup.usermicroservice.domain.model.Category;
import com.pragma.powerup.usermicroservice.domain.model.Dish;
import com.pragma.powerup.usermicroservice.domain.model.Restaurant;
import com.pragma.powerup.usermicroservice.domain.spi.IDishPersistencePort;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DishUserCaseTest {

    @Mock
    private IDishPersistencePort dishPersistencePort;

    @Mock
    private IPlazoletaClienteRest plazoletaClienteRest;
    @InjectMocks
    private DishUseCase dishUseCase;
    private Dish dish, dishWhithoutRestaurant, dishUpdated;
    private Restaurant restaurant, restaurantNull;
    private Category category;

    private UserResponseDto user;

    @Captor
    private ArgumentCaptor<Long> restaurantIdCaptor;


    /**
     *
     Set up method executed before each test case.
     */
    @BeforeEach
    void setUp() {

        MockitoAnnotations.openMocks(this);
        dishUseCase =new DishUseCase(dishPersistencePort, plazoletaClienteRest );
        restaurant = new Restaurant(1L,"restaurant-A", 123L, "local-36",+573557865L,"url", 1L);
        category =new Category(1L, "lunch", "Almuerzos");
        dish = new Dish();
        dish.setId(1L);
        dish.setNameDish("bandeja");
        dish.setPrice(12000L);
        dish.setDescription("Badeja Paisa");
        dish.setUrlImage("URL");
        dish.setRestaurant(restaurant);
        dish.setCategory(category);

        restaurantNull=new Restaurant();
        dishWhithoutRestaurant=new Dish();
        dishWhithoutRestaurant.setId(1L);
        dishWhithoutRestaurant.setNameDish("frijoles");
        dishWhithoutRestaurant.setPrice(1000L);
        dishWhithoutRestaurant.setDescription("frijoles");
        dishWhithoutRestaurant.setUrlImage("url.image");
        dishWhithoutRestaurant.setRestaurant(restaurantNull);
        dishWhithoutRestaurant.setCategory(category);

        dishUpdated= new Dish();
        dishUpdated.setId(dish.getId());
        dishUpdated.setNameDish(dish.getNameDish());
        dishUpdated.setPrice(15000L);
        dishUpdated.setDescription("Plato tipico");
        dishUpdated.setUrlImage("url.image");
        dishUpdated.setRestaurant(restaurant);
        dishUpdated.setCategory(category);

        user = new UserResponseDto(1L, 1L, 3L);

    }

    /**
     *
     Unit test to verify the behavior of the saveDish method.
     */

    @Test
    void saveDish() {

        when(plazoletaClienteRest.findById(eq(1L))).thenReturn(user);

        ArgumentCaptor <Dish> dishArgumentCaptor = ArgumentCaptor.forClass(Dish.class);

        dishUseCase.saveDish(dish);

        verify(dishPersistencePort).saveDish(dishArgumentCaptor.capture());

        Dish captureDish = dishArgumentCaptor.getValue();

        assertEquals(dish.getId(), captureDish.getId());
    }

    /**
     *
     Unit test to verify that a RestaurantEmptyException is thrown when the restaurant ID is null.
     */
    @Test
    void returnRestaurantEmptyExceptionWhenRestaurantIdIsNull(){

        Assertions.assertThrows(RestaurantEmptyException.class, () ->{
            dishUseCase.saveDish(dishWhithoutRestaurant);
        });
    }

    /**
     *
     Unit test to verify the update of a dish.
     */

    @Test
    void updateDish() {

        ArgumentCaptor<Dish> dishArgumentCaptor = ArgumentCaptor.forClass(Dish.class);

        ArgumentCaptor<Long> idArgumentCaptor = ArgumentCaptor.forClass(Long.class);

        dishUseCase.updateDish(dish, dish.getId());

        verify(dishPersistencePort).updateDish(dishArgumentCaptor.capture(), idArgumentCaptor.capture());

        Dish captureDish = dishArgumentCaptor.getValue();

        Long captureId = idArgumentCaptor.getValue();

        assertEquals(dish.getId(), captureDish.getId());

    }

    /**
     * Unit test to verify the behavior of the
     * {@code findByRestaurantId} method in the {@code DishUseCase} class.
     */

    @Test
    void findByRestaurantId() {


        Long restaurantId = 1L;
        int page = 0;
        List<Dish> expectedDishes = Arrays.asList(new Dish(), new Dish());
        when(dishPersistencePort.findByRestaurantId(restaurantIdCaptor.capture(), eq(page)))
                .thenReturn(expectedDishes);


        List<Dish> result = dishUseCase.findByRestaurantId(restaurantId, page);


        assertEquals(expectedDishes, result);

        Long capturedRestaurantId = restaurantIdCaptor.getValue();
        assertEquals(restaurantId, capturedRestaurantId);
        verify(dishPersistencePort).findByRestaurantId(capturedRestaurantId, page);
    }
}
