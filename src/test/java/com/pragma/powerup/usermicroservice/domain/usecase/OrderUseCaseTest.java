package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RestaurantEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.utils.StateOrder;
import com.pragma.powerup.usermicroservice.domain.model.Dish;
import com.pragma.powerup.usermicroservice.domain.model.Order;
import com.pragma.powerup.usermicroservice.domain.spi.IOrderPersistencePort;
import com.pragma.powerup.usermicroservice.domain.utils.State;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderUseCaseTest {

    @Mock
    private IOrderPersistencePort orderPersistencePort;

    @InjectMocks
    private OrderUseCase orderUseCase;

    @Captor
    private ArgumentCaptor<Order> orderCaptor;

    @Captor
    private ArgumentCaptor<Long> restaurantIdCaptor;
    @Captor
    private ArgumentCaptor<StateOrder> stateCaptor;
    @Captor
    private ArgumentCaptor<Integer> pageCaptor;

    private Order order;
    private List<Dish> dishes;
    private State state;
    private LocalDate orderDate;
    private RestaurantEntity restaurant;

    private List<Order> orders;

    private Long restaurantId;

    private StateOrder stateOrder;

    private int page;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        orderUseCase = new OrderUseCase(orderPersistencePort);
        dishes = new ArrayList<>();
        state = State.PENDING;
        restaurant = new RestaurantEntity();
        orderDate = LocalDate.now();
        order = new Order(1l, 1l, orderDate, state, restaurant, 2L, dishes, (long) dishes.size());
        orders = new ArrayList<>();
        restaurantId = 1L;
        stateOrder = StateOrder.PENDING;
        page = 1;

    }

    @Test
    void createOrder() {

        orderUseCase.createOrder(order);
        verify(orderPersistencePort).createOrder(orderCaptor.capture());
        Order capturedOrder = orderCaptor.getValue();
        assertEquals(order.getDishes(), capturedOrder.getDishes());
        assertEquals(State.PENDING, capturedOrder.getState());
        assertEquals((long) order.getDishes().size(), capturedOrder.getQuantity());


    }

    @Test
    void findByRestaurantIdAndState() {

        when(orderPersistencePort.findByRestaurantIdAndState(restaurantId, stateOrder, page)).thenReturn(orders);

        List<Order> result = orderUseCase.findByRestaurantIdAndState(restaurantId, stateOrder, page);

        verify(orderPersistencePort).findByRestaurantIdAndState(restaurantIdCaptor.capture(), stateCaptor.capture(), pageCaptor.capture());
        assertEquals(restaurantId, restaurantIdCaptor.getValue());
        assertEquals(stateOrder, stateCaptor.getValue());
        assertEquals(page, pageCaptor.getValue());
        assertEquals(orders, result);


    }
}