package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.adapters.driving.http.clients.IPlazoletaClienteRest;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.domain.exceptions.RoleNotAllowedForCreationException;
import com.pragma.powerup.usermicroservice.domain.exceptions.UserNotFoundException;
import com.pragma.powerup.usermicroservice.domain.model.Restaurant;
import com.pragma.powerup.usermicroservice.domain.spi.IRestaurantPersistencePort;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RestaurantUseCaseTest {

    @Mock
    private IRestaurantPersistencePort restaurantPersistencePort;
    @Mock
    private IPlazoletaClienteRest client;
    @InjectMocks
    private RestaurantUseCase restaurantUseCase;
    private UserResponseDto user, userWhitoutRoleOwner;
    private Restaurant restaurant, restaurantWhitoutOwner;

    /**
     * Set up method executed before each test in the test class.
     It initializes the necessary objects and dependencies for the tests.
     */

    @BeforeEach
    void setUp() {

        MockitoAnnotations.openMocks(this);
        restaurantUseCase=new RestaurantUseCase(restaurantPersistencePort, client );
        user = new UserResponseDto(1L, 1L, 3L);
        restaurant= new Restaurant(1L,"restaurant-A", 123L, "local-36",+573557865L,"url", 1L);
        userWhitoutRoleOwner = new UserResponseDto(2L, 2L, 1L);

        restaurantWhitoutOwner= new Restaurant(1L,"restaurant-A", 123L, "local-36",+573557865L,"url", 2L);
    }

    /**
     Unit test for the saveRestaurant() method.
     It verifies that the restaurant is saved correctly by capturing the argument passed to the saveRestaurant() method.
     */

    @Test
    void saveRestaurant() {

        when(client.findById(ArgumentMatchers.eq(1L))).thenReturn(user);

        ArgumentCaptor <Restaurant> restaurantArgumentCaptor = ArgumentCaptor.forClass(Restaurant.class);

        restaurantUseCase.saveRestaurant(restaurant);

        verify(restaurantPersistencePort).saveRestaurant(restaurantArgumentCaptor.capture());

        Restaurant captureRestaurant=restaurantArgumentCaptor.getValue();

        assertEquals(restaurant.getId(),captureRestaurant.getId());
    }
    /**
     Unit test for the scenario where a UserNotFoundException is thrown
     when the user obtained from client.findById() is null.
     */
    @Test
    void returnUserNotFoundExceptionwhenUserIsnull() {

        when(client.findById(ArgumentMatchers.eq(1L))).thenReturn(null);

        Assertions.assertThrows(UserNotFoundException.class, () -> {
            restaurantUseCase.saveRestaurant(restaurant);
        });
    }

    /**
     Unit test for the scenario where a RoleNotAllowedForCreationException is thrown
     when the user's role ID does not match with the expected role ID (3).
     */
    @Test
    void returnRoleNotAllowedForCreationExceptionWhenUsersIdRoleNotMatchWithThree() {

        when(client.findById(ArgumentMatchers.eq(2L))).thenReturn(userWhitoutRoleOwner);

        Assertions.assertThrows(RoleNotAllowedForCreationException.class, () -> {
            restaurantUseCase.saveRestaurant(restaurantWhitoutOwner);
        });

    }

    /**
     * Unit test for the findAll() method in the RestaurantUseCase class.
     * It verifies that the list of restaurants is retrieved correctly
     * by mocking the restaurantPersistencePort and asserting the result.
     */

    @Test
    void findAll() {
        int page = 0;
        List<Restaurant> expectedRestaurantList = Arrays.asList(restaurant);
        when(restaurantPersistencePort.findAll(page)).thenReturn(expectedRestaurantList);

        List<Restaurant> result = restaurantUseCase.findAll(page);

        assertEquals(expectedRestaurantList, result);
        verify(restaurantPersistencePort).findAll(page);


    }
}